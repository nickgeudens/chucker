export class Account
  {
  id: number;
  username: string;
  password: string;
  activated: boolean
  email: string;
  first_name: string
  last_name: string
  profile_pic_url: string = '/assets/placeholder.png';
  creation_date: Date
  }