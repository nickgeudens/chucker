import { CardService } from './../../shared/services/card.service';
import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.css']
})
export class AddCardComponent implements OnInit {

  @Output() updated = new EventEmitter();
  addMode:boolean;
  title:string;
  description:string;

  constructor(private cardserv:CardService) {

  }

  ngOnInit() {

  }

  addCard(){
    this.cardserv.getRandomChuck().subscribe((chuck)=>{
      this.cardserv.addCard("\""+chuck.value.joke.replace(/&quot;/g, "\"")+"\"","Chuck Norris").subscribe(()=>
    {
      this.addMode=false,
      this.updated.emit();
    });
    })
    
  }

}
