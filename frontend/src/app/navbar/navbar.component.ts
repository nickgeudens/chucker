import { Account } from './../../models/account';
import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { AccountService } from '../shared/services/account.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private router:Router,private http:HttpClient, public accoutService:AccountService) {
    accoutService.profilePictureChanged.subscribe(()=>
      this.pictureUrl=this.pictureUrl + "?"+new Date().getTime()
    )
  }
  account: Account;
  pictureUrl="/api/profile_pic"
  login = false;
  down = false;
  fulldown = false;

  ngOnInit() {
    this.getAccount();
    this.accoutService.checkAuthenticated();
  }

  closeDropdown(){
    this.down=false;
  }

  toggleDropdown(){
    this.down=!this.down;
  }

  toggleFullDropdown(){
    this.fulldown=!this.fulldown;
  }

  closeAllDropdown(){
    this.down=false;
    this.fulldown=false;
  }

  logout(){
    this.accoutService.logout();
    this.accoutService.authenticated=false;
  }

  getAccount(){
    this.accoutService.getAccount().subscribe(data =>{
      this.account=data
    });
  }

}
