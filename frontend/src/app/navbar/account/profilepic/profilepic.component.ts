import { AccountService } from './../../../shared/services/account.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
@Component({
  selector: 'app-profilepic',
  templateUrl: './profilepic.component.html',
  styleUrls: ['./profilepic.component.css']
})
export class ProfilepicComponent implements OnInit {
  public uploader: FileUploader;
  private hasDragOver = false;

  @Input()
  editmode = true;

  @Input()
  url = '/api/profile_pic';

  @Output()
  urlChange = new EventEmitter();

  constructor() {
    this.uploader = new FileUploader({
      url: '/api/upload_profile_pic',
      disableMultipart: false,
      autoUpload: true
    });

    this.uploader.response.subscribe(res => {
      this.url='/api/profile_pic?'+ new Date().getTime();
      this.urlChange.emit();
    });
  }

  public fileOver(e: any): void {
    this.hasDragOver = e;
  }

  ngOnInit() {
  }

}