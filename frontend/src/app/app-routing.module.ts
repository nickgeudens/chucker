import { NgModule } from '@angular/core';
import { HomeComponent} from './home/home.component';
import { RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './navbar/login/login.component';
import {AccountComponent} from './navbar/account/account.component';
import {SettingsComponent} from './navbar/settings/settings.component';
import {RegisterComponent} from "./navbar/register/register.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CardsTabComponent } from './cards-tab/cards-tab.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent},
  { path: 'account', component: AccountComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'cards', component: CardsTabComponent},
  { path: 'register', component: RegisterComponent},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
