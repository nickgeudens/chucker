import { CardService } from './shared/services/card.service';

import { AccountService } from './shared/services/account.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './navbar/login/login.component';
import { RegisterComponent } from './navbar/register/register.component';
import { AppRoutingModule } from './app-routing.module';
import { AccountComponent } from './navbar/account/account.component';
import { SettingsComponent } from './navbar/settings/settings.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ProfilepicComponent } from './navbar/account/profilepic/profilepic.component';
import { FileUploadModule } from 'ng2-file-upload';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CardsTabComponent } from './cards-tab/cards-tab.component';
import { CardComponent } from './cards-tab/card/card.component';
import { AddCardComponent } from './cards-tab/add-card/add-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    AccountComponent,
    SettingsComponent,
    ProfilepicComponent,
    PageNotFoundComponent,
    CardComponent,
    AddCardComponent,
    CardsTabComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FileUploadModule,
    ReactiveFormsModule
  ],
  providers: [AccountService, CardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
