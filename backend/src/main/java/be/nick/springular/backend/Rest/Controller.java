package be.nick.springular.backend.Rest;

import be.nick.springular.backend.Security.AuthoritiesConstants;
import be.nick.springular.backend.Security.User.ThumbnailService;
import be.nick.springular.backend.Security.User.User;
import be.nick.springular.backend.Security.User.UserRepository;
import be.nick.springular.backend.Security.User.UserService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.net.URLDecoder;
import java.util.List;

@RestController
@RequestMapping("/api")
public class Controller {
    private final UserRepository userRepository;
    private final UserService userService;
    private final ThumbnailService thumbnailService;
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    public Controller(UserRepository userRepository, UserService userService, ThumbnailService thumbnailService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.thumbnailService = thumbnailService;
    }

    @RequestMapping("/authenticated")
    @ResponseBody
    boolean isAuthenticated(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.isAuthenticated() && !(authentication instanceof AnonymousAuthenticationToken);
    }

    @RequestMapping("/users/create")
    @ResponseStatus(HttpStatus.CREATED)
    void createUser(@RequestBody User appUser){
    userService.registerUser(appUser);
    }

    @Secured(AuthoritiesConstants.ADMIN)
    @RequestMapping("/users")
    @ResponseBody
    List<User> getUser(){
        return userRepository.findAll();
    }

    @Secured(AuthoritiesConstants.USER)
    @RequestMapping("/userdetails")
    @ResponseBody
    User getUserDetails(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)){
            User user = userRepository.findOneByUsername(authentication.getName()).get();
            user.setPassword("[PROTECTED]");
            return user;
        }
        return null;
    }

    @Secured(AuthoritiesConstants.USER)
    @PostMapping("/upload_profile_pic")
    public ResponseEntity uploadProfilePic( @RequestParam("file") MultipartFile file){
        String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)){
            User user = userRepository.findOneByUsername(authentication.getName()).get();
            user.setProfile_pic(thumbnailService.makeThumbnail(file));
            userRepository.save(user);
        }
        return ResponseEntity.ok().build();
    }

    @Secured(AuthoritiesConstants.USER)
    @GetMapping("/profile_pic")
    public ResponseEntity<Resource> getProfilePic(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)){
            User user= userRepository.findOneByUsername(authentication.getName()).get();
            if(user.getProfile_pic()!=null){
                Resource file = new ByteArrayResource(user.getProfile_pic());
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION)
                        .body(file);
            }
        }
        return ResponseEntity.ok().build();
    }

    @Secured(AuthoritiesConstants.USER)
    @PutMapping("/users/update")
    ResponseEntity updateUser(@Valid @RequestBody User appUser){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)){
            User user = userRepository.findOneByUsername(authentication.getName()).get();
            user.setFirst_name(appUser.getFirst_name());
            user.setLast_name(appUser.getLast_name());
            user.setEmail(appUser.getEmail());
            user.setFirst_name(appUser.getFirst_name());
            user.setUsername(appUser.getUsername());
            userRepository.save(user);
        }
        return ResponseEntity.accepted().build();
    }

    @GetMapping("/users/username_exists")
    boolean checkIfUsernameExists(@RequestParam String username){
        return userRepository.findOneByUsername(username).isPresent();
    }
    @GetMapping("/users/email_exists")
    boolean checkIfEmailExists(@RequestParam String email){
        return userRepository.findOneByEmail(URLDecoder.decode(email)).isPresent();
    }

}
